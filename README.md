# Application Blueprint for Nextcloud

## What is Nextcloud?
Nextcloud is a self-hosted productivity platform that keeps you in control.

**Official Website:** [nextcloud.com](https://nextclout.com)


This application blueprint uses the [official nextcloud](https://hub.docker.com/_/nextcloud) Docker image.

## Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

### Details
To simplify the deployment process, we have set default values for several required environment variables and hidden them from the UI. The remaining environment variables are exposed as the following “Detail” fields, which you must fill in after selecting a deployment:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| SMTP Host                     | (none)           |
| SMTP Name                     | (none)           |
| SMTP Password                 | (none)           |
| Mail From Address             | (none)           |
| Mail Options Auth User        | (none)           |
| Mail Options Auth Pass        | (none)           |
| Redis Host Password           | (auto-generated/when applicable) |
| Postgres Password             | (auto-generated/when applicable) |

Below is a list of the hidden default environment variables and their values. As explained above, you will not encounter these environment variables as you fill in the deployment blueprint:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| Database User                 | (auto-generated/when applicable) |
| Database Host                 | (auto-generated/when applicable) |
| Database Password             | (auto-generated/when applicable) |
| Redis Host                    | (auto-generated/when applicable) |


### Components
Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. After you select a deployment blueprint, you will be prompted to fulfill the component requirements and configure the deployment to your needs. Common components include Compute, DNS, Database, and Mail Server:

#### Compute

Nextcloud requires the following compute resources:

- At least 1 CPU
- At least 1024MB of RAM
- At least 16GB of hard disk space (allocating additional storage is recommended when not creating a database resource)

#### DNS

A DNS provider must be specified for the deployed site to be accessible via a domain name.

Supported DNS providers are:

- Google Cloud DNS
- DigitalOcean DNS
- AWS Route53
- Azure DNS

All providers require a domain name to use. ***Note:** the domain must be registered to that service.*

The `Subdomain` input above will be used to register a new subdomain under the given domain. For example, given the subdomain `nextcloud` and domain zone `mysite.com`, the site will be accessible at `nextcloud.mysite.com`.

#### Database: PostgresDB (recommended extra)

For this blueprint, Nextcloud can use either of these databases:

| Database | Supported Versions |
|----------|--------------------|
| PostgresDB  | `10+`           |
| SQLite  | included (default)       |

For additional compatibility information, see [Nextcloud's system requirements](https://docs.nextcloud.com/server/latest/admin_manual/installation/system_requirements.html).

SQLite is used when a PostgresDB is not configured.  SQLite can be used for development or a deployment with a small number of users.  Some features of Nextcloud may not be fully functional under SQLite.

#### Cache: Redis (optional extra)

Nextcloud can be configured to use a Redis instance as a cache.  Alternatively a Nextcloud deployment will use a local APCu cache.  See [Memory caching](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/caching_configuration.html) for more details.

#### Mail Server (optional extra)

Nextcloud can send emails to complete user registration and allow users to reset their passwords.

Two mail providers are supported, with their needed inputs detailed below:

<!-- use <br> for multi-line tables, GFM does not support multi-line tables -->

**SendGrid:**

| Input   | Description                                                                                    |
|---------|------------------------------------------------------------------------------------------------|
| API Key | This can be created at [SendGrid account settings](https://app.sendgrid.com/settings/api_keys) |

**Generic SMTP Server:**

| Input     | Description                                                                     |
|-----------|---------------------------------------------------------------------------------|
| SMTP Host | Name of the mail server, e.g. `smtp.someprovider.com`                           |
| User Name | Usually the email to send as. Double check the documentation for your provider! |
| Secret    | The password or login token to authenticate with.                               |
| Protocol  | Either `ssl` or `tls`, probably `tls`.                                          |

**Gmail:**

Use Generic SMTP Server with the following inputs:

| Input     | Value                                       |
|-----------|---------------------------------------------|
| SMTP Host | `smtp.gmail.com`                            |
| User Name | Gmail username (email without `@gmail.com`) |
| Secret    | Gmail password                              |
| Protocol  | `tls`                                       |


#### Required vs “Extras”
Some components are required for a deployment to succeed. Others, found under the “Extras” tab, are optional resources that enhance the features or performance of your deployment.

